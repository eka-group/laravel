<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class InertiaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCache();

        $this->registerSharedProperties();
    }

    protected function registerCache()
    {
        Inertia::version(fn () => md5_file(public_path('mix-manifest.json')));
    }

    protected function registerSharedProperties()
    {
        Inertia::share([
            'app' => fn () => ['name' => Config::get('app.name')],
        ]);
    }
}
