<?php

namespace App\Repositories\Contracts;

interface RepositoryInterface
{
    public function create(array $data);

    public function findBy($field, $value, $columns = ['*']);

    public function update($id, array $data);

    public function delete($id);
}
