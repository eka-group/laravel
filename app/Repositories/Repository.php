<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

abstract class Repository implements RepositoryInterface
{
    public $model;

    private $query;

    public function __construct()
    {
        $this->query = $this->model::query();
    }

    public function __call($method, $parameters)
    {
        $result = $this->query->{$method}(...$parameters);

        if ($result instanceof Builder) {
            $this->query = $result;

            return $this;
        }

        return $result;
    }

    public static function __callStatic($method, $parameters)
    {
        return (new static)->{$method}(...$parameters);
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function findBy($field, $value, $columns = ['*'])
    {
        return $this->model::where($field, $value)->first($columns);
    }

    public function update($model, array $data)
    {
        return $model->update($data);
    }

    public function delete($model)
    {
        return $model->delete();
    }
}
